package net.scrumplex.voiz.server

import java.nio.ByteBuffer
import java.nio.charset.Charset
import kotlin.math.min

fun ByteBuffer.remainingArray(): ByteArray {
    val array = ByteArray(remaining())
    get(array)
    return array
}

fun ByteBuffer.getString(maxLength: Int, charset: Charset = Charsets.UTF_8): String {
    val stringBytes = ByteArray(maxLength)

    for (i in 0 until min(remaining(), maxLength)) {
        stringBytes[i] = get()
        if (stringBytes[i] == 0x00.toByte())
            return String(stringBytes, 0, i, charset)
    }
    return String(stringBytes, charset)
}
