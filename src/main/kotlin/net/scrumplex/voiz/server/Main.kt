package net.scrumplex.voiz.server

import net.scrumplex.voiz.server.net.Server
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService

val scheduledPool: ScheduledExecutorService = Executors.newScheduledThreadPool(4)

fun main() {
    val localServer = LocalServer(Server(1234))
    localServer.clientManager.trackConnections()
    localServer.server.listen()
}