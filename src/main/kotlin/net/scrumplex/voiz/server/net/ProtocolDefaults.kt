package net.scrumplex.voiz.server.net

object ProtocolDefaults {

    const val DEFAULT_PORT = 1234

    const val CONTROLV0_BUFFER = 256
    const val CONTROLV0_TIMEOUT = 60 * 1000 // Time in ms
    val CONTROLV0_HEADER = byteArrayOf(0x56, 0x5A, 0x43, 0x00) // VZC0
    val CONTROLV0_CHARSET = Charsets.UTF_8


    const val VOICEV0_BUFFER = 1024

    val VOICEV0_HEADER = byteArrayOf(0x56, 0x5A, 0x56, 0x00) // VZV0
}
