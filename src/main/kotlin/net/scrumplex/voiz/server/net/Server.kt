package net.scrumplex.voiz.server.net

import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetSocketAddress
import java.net.SocketAddress
import java.nio.ByteBuffer

class Server(socketAddress: SocketAddress) {

    constructor(port: Int) : this(InetSocketAddress(port))

    companion object {
        const val DEFAULT_PACKET_BUFFER = 1024
    }

    private val socket = DatagramSocket(socketAddress)
    private val packetHandlers = arrayListOf<PacketHandler>()

    fun listen() {
        while (socket.isBound) {
            val data = ByteArray(DEFAULT_PACKET_BUFFER)
            val datagramPacket = DatagramPacket(data, data.size)
            socket.receive(datagramPacket)

            val handler = packetHandlers.find { it.identifyPacket(data) }
                ?: continue

            //println(handler.toString())

            val packet = Packet(this, datagramPacket)

            handler.handlePacket(packet)

            if (handler.consume)
                packetHandlers.remove(handler)
        }
    }

    fun send(datagramPacket: DatagramPacket) {
        socket.send(datagramPacket)
    }

    fun addPacketHandler(handler: PacketHandler) {
        packetHandlers.add(handler)
    }


    class Packet(private val server: Server, val datagramPacket: DatagramPacket) {
        val buffer = ByteBuffer.wrap(datagramPacket.data)

        fun respond(data: ByteBuffer) {
            val responsePacket = DatagramPacket(data.array(), data.remaining())
            responsePacket.socketAddress = datagramPacket.socketAddress
            server.send(responsePacket)
        }
    }

    interface PacketHandler {

        /**
         * Defines if packet handler should be discarded after a packet was handled successfully.
         */
        val consume: Boolean

        fun identifyPacket(data: ByteArray): Boolean

        fun handlePacket(packet: Packet)

    }

}
