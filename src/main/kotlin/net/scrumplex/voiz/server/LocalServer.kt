package net.scrumplex.voiz.server

import net.scrumplex.voiz.server.net.ProtocolDefaults
import net.scrumplex.voiz.server.net.Server
import java.nio.ByteBuffer
import kotlin.math.min

class LocalServer(val server: Server) {

    private val serverName = "scrumplex.net"
    private val serverNameBytes = ByteArray(32)

    val clientManager = ClientManager(this)

    init {
        System.arraycopy(
            serverName.toByteArray(ProtocolDefaults.CONTROLV0_CHARSET),
            0,
            serverNameBytes,
            0,
            min(serverNameBytes.size, serverName.length)
        )

        server.addPacketHandler(VoicePacketHandler(this))
        server.addPacketHandler(HelloPacketHandler(this))
        server.addPacketHandler(HandshakePacketHandler(this))
        server.addPacketHandler(HeartbeatPacketHandler(this))
    }

    class HelloPacketHandler(private val localServer: LocalServer) : Server.PacketHandler {
        override val consume = false

        private val packetHeader = ProtocolDefaults.CONTROLV0_HEADER // VZC.
        private val packetMode = byteArrayOf(0x43, 0x53, 0x00, 0x00) // CS00 - client2server hello

        override fun identifyPacket(data: ByteArray): Boolean {
            return data.size >= 8 // minimum size to contain header and packet mode
                    && data.sliceArray(0 until 4).contentEquals(packetHeader)
                    && data.sliceArray(4 until 8).contentEquals(packetMode)
        }

        override fun handlePacket(packet: Server.Packet) {
            packet.buffer.position(8) //  skip header and mode

            val response = ByteBuffer.allocate(ProtocolDefaults.CONTROLV0_BUFFER)
            response.put(ProtocolDefaults.CONTROLV0_HEADER)
            response.put(0x53)
            response.put(0x43)
            response.put(0x00)
            response.put(0x00)
            response.put(localServer.serverNameBytes)
            response.flip()

            packet.respond(response)
        }
    }

    class HandshakePacketHandler(private val localServer: LocalServer) : Server.PacketHandler {
        override val consume = false

        private val packetHeader = ProtocolDefaults.CONTROLV0_HEADER // VZC.
        private val packetMode = byteArrayOf(0x43, 0x53, 0x00, 0x01) // CS01 - client2server handshake

        override fun identifyPacket(data: ByteArray): Boolean {
            return data.size >= 11 // minimum size to contain header, packet mode and minimum username size of 3 bytes
                    && data.sliceArray(0 until 4).contentEquals(packetHeader)
                    && data.sliceArray(4 until 8).contentEquals(packetMode)
        }

        override fun handlePacket(packet: Server.Packet) {
            packet.buffer.position(8) //  skip header and mode

            if (localServer.clientManager.maximumClientsReached) {
                // TODO: error packet
                return
            }

            val client =
                localServer.clientManager.createClient(packet.datagramPacket.socketAddress, packet.buffer.getString(32))

            val response = ByteBuffer.allocate(256)
            response.put(ProtocolDefaults.CONTROLV0_HEADER)
            response.put(0x53)
            response.put(0x43)
            response.put(0x00)
            response.put(0x01)
            response.putShort(client.id)
            response.flip()

            packet.respond(response)
        }
    }

    class HeartbeatPacketHandler(private val localServer: LocalServer) : Server.PacketHandler {
        override val consume = false

        private val packetHeader = ProtocolDefaults.CONTROLV0_HEADER // VZC.
        private val packetMode = byteArrayOf(0x43, 0x53, 0x00, 0x02) // CS02 - client2server heartbeat

        override fun identifyPacket(data: ByteArray): Boolean {
            return data.size >= 10 // minimum size to contain header, packet mode and clientId
                    && data.sliceArray(0 until 4).contentEquals(packetHeader)
                    && data.sliceArray(4 until 8).contentEquals(packetMode)
        }

        override fun handlePacket(packet: Server.Packet) {
            packet.buffer.position(8) //  skip header and mode

            val clientId = packet.buffer.short

            if (clientId < 0)
                return // drop invalid id


            localServer.clientManager.heartbeat(clientId)

            val response = ByteBuffer.allocate(ProtocolDefaults.CONTROLV0_BUFFER)
            response.put(ProtocolDefaults.CONTROLV0_HEADER)
            response.put(0x53)
            response.put(0x43)
            response.put(0x00)
            response.put(0x02)
            response.flip()

            packet.respond(response)
        }
    }

    // TODO: refactor into relevant class
    class VoicePacketHandler(private val localServer: LocalServer) : Server.PacketHandler {
        override val consume = false

        private val packetHeader = ProtocolDefaults.VOICEV0_HEADER // VZV.

        override fun identifyPacket(data: ByteArray): Boolean {
            return data.size >= 10 // minimum size to contain header, packet mode and clientId
                    && data.sliceArray(0 until 4).contentEquals(packetHeader)
        }

        override fun handlePacket(packet: Server.Packet) {
            val clientId = packet.buffer.getShort(4)

            localServer.clientManager.distributeVoicePacket(clientId, packet.buffer)
        }
    }
}
