package net.scrumplex.voiz.server

import net.scrumplex.voiz.server.net.ProtocolDefaults
import java.io.IOException
import java.net.DatagramPacket
import java.net.SocketAddress
import java.nio.ByteBuffer
import java.util.concurrent.TimeUnit

class ClientManager(private val localServer: LocalServer) {

    private val maxClients = 16

    val maximumClientsReached: Boolean
        get() = clientIds.size == maxClients.toInt()


    private var clientIds = mutableListOf<Short>()
    private var connectedClients = mutableListOf<ConnectedClient>()

    fun createClient(address: SocketAddress, username: String): ConnectedClient {
        val client = ConnectedClient(generateClientId(), address, username)
        clientIds.add(client.id)
        connectedClients.add(client)
        heartbeat(client.id)
        println("Created client with id ${client.id} and name ${client.username}")
        return client
    }

    fun heartbeat(clientId: Short) {
        connectedClients.find {
            it.id == clientId
        }?.updateLastPacket()
    }

    fun distributeVoicePacket(clientId: Short, buffer: ByteBuffer) {
        val distributionPacket = DatagramPacket(buffer.array(), 0, buffer.remaining())

        val clients = connectedClients.filter {
            it.id != clientId
        }

        if (connectedClients.size > clients.size) { // check if clientId existed in the first place
            for (target in clients) {
                distributionPacket.socketAddress = target.address
                try {
                    localServer.server.send(distributionPacket)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }


    private fun generateClientId(): Short {
        if (clientIds.size == maxClients)
            return -1
        for (i in 0 until maxClients) {
            if (!clientIds.contains(i.toShort())) {
                return i.toShort()
            }
        }
        return -1
    }


    fun trackConnections() {
        scheduledPool.scheduleAtFixedRate({
            try {
                connectedClients.filter {
                    System.currentTimeMillis() - it.lastPacket >= ProtocolDefaults.CONTROLV0_TIMEOUT
                }.forEach {
                    clientIds.remove(it.id)
                    connectedClients.remove(it)
                }

                println("Clients connected: ${connectedClients.size}")
                for (client in connectedClients) {
                    println(" - ${client.username} id: ${client.id}: last heartbeat: ${client.lastPacket}")
                }
                println()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }, 0, 1, TimeUnit.SECONDS)
    }

    class ConnectedClient(val id: Short, val address: SocketAddress, val username: String) {

        var lastPacket: Long = 0
            private set

        fun updateLastPacket() {
            lastPacket = System.currentTimeMillis()
        }

    }
}